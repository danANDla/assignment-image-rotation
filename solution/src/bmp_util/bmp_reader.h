#ifndef BMP_READER_H
#define BMP_READER_H
#include "../image/image.h"
#include <stdio.h>

enum read_status {
	READ_OK = 0,
	READ_INVALID_SIGNATURE,
	READ_INVALID_BITS,
	READ_INVALID_HEADER
};

enum read_status from_bmp(FILE* in, struct image* img);

#endif // !BMP_READER_H

