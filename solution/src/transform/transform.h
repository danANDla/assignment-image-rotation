#ifndef TRANSFORM_H
#define TRANSFORM_H

#include "../image/image.h"

struct transform_result{
    enum transform_status{
        TRANSFORM_OK = 0,
        TRANSFORM_ERROR,
        UNKNOWN_TRANSFORMATION
    } type;
    struct image img;
};


enum transformations{
    CLOCKWISE_90 = 0,
    COUNTERCLOCKWISE_90
};

struct transform_result transform_image(struct image* source, enum transformations type);
#endif // !TRANSFORM_H

